// vim:set sw=2 ts=2 sts=2 expandtab:
const replace = [
  {
    "name": "Darmanin",
    "replaceWith": "Darmalsain"
  },
  {
    "name": "Bruno Le Maire|Bruno Lemaire",
    "replaceWith": "Bruno La Merde"
  },
  {
    "name": "Jean Castex",
    "replaceWith": "JEANG CASTEX"
  },
  {
    "name": "Macron",
    "replaceWith": "Micron"
  },
  {
    "name": "Barbara Pompili",
    "replaceWith": "Barbara Pump-it-up"
  },
  {
    "name": "Jean-Michel Blanquer",
    "replaceWith": "Chiant-Michiel Planqué"
  },
  {
    "name": "Marlène Schiappa",
    "replaceWith": "Dame Gourdasse"
  },
  {
    "name": "Élisabeth Borne|Elisabeth Borne",
    "replaceWith": "Élisabeth MilBornes"
  },
  {
    "name": "Dupond-Moretti",
    "replaceWith": "Dupond-Moriarty"
  },
  {
    "name": "Jean-Yves Le Drian|Jean Yves Le Drian|Jean-Yves le Drian",
    "replaceWith": "Jean-Yves Le Vrillant"
  },
  {
    "name": "Agnès Pannier-Runacher",
    "replaceWith": "Agnès Panier-Panaché"
  },
  {
    "name": "Roselyne Bachelot",
    "replaceWith": "Roselyne Cachalot"
  },
  {
    "name": "Olivier Véran",
    "replaceWith": "Olivier Variant"
  },
  {
    "name": "Frédérique Vidal|Frederique Vidal",
    "replaceWith": "Frédérique La Vide"
  },
  {
    "name": "Amélie de Montchalin|Amélie De Montchalin",
    "replaceWith": "Amélie de Mon chalet à la Montagne"
  },
  {
    "name": "président|Président",
    "replaceWith": "dictateur bienveillant"
  },
  {
    "name": "ministre|Ministre",
    "replaceWith": "Sinistre"
  },
  {
    "name": "ministres|Ministres",
    "replaceWith": "Sinistres"
  },
  {
    "name": "député|Député",
    "replaceWith": "Dépité"
  },
  {
    "name": "députés|Députés",
    "replaceWith": "Dépités"
  },
  {
    "name": "sénateur|Sénateur",
    "replaceWith": "Sécateur"
  },
  {
    "name": "sénateurs|Sénateurs",
    "replaceWith": "Sécateurs"
  },
  {
    "name": "Trump",
    "replaceWith": "Trumpet"
  },
  {
    "name": "sarkozy|Sarkozy",
    "replaceWith": "p'tite crotte"
  },
  {
    "name": "Jean-Marie Le Pen",
    "replaceWith": "Jean-Marie Le Vieux Facho"
  },
  {
    "name": "Marine Le Pen",
    "replaceWith": "Marine Malice"
  },
  {
    "name": "Éric Ciotti|Eric Ciotti",
    "replaceWith": "Éric Pacotille"
  },
  {
    "name": "Mélenchon",
    "replaceWith": "Méluche"
  },
  {
    "name": "Castaner",
    "replaceWith": "Grosse Merde"
  },
  {
    "name": "Zemmour",
    "replaceWith": "ZigHail"
  },
  {
    "name": "Bolloré",
    "replaceWith": "Boloss-ré"
  },
  {
    "name": "Bezos",
    "replaceWith": "Pesos"
  },
  {
    "name": "Bernard Arnaud",
    "replaceWith": "Bernard Legrorichou"
  },
  {
    "name": "Chalençon",
    "replaceWith": "Charançon"
  },
  {
    "name": "Fabien Roussel",
    "replaceWith": "Fabien Condé-Roussel"
  },
  {
    "name": "Anne Hidalgo",
    "replaceWith": "Annie d'Algo"
  },
  {
    "name": "Taubira",
    "replaceWith": "Teubira"
  },
  {
    "name": "Poutine",
    "replaceWith": "Raspoutine"
  },
  {
    "name": "Pénicaud",
    "replaceWith": "Pémican"
  },
];
/* For copy/paste
  {
    "name": "",
    "replaceWith": ""
  },
*/

// Create array of regexps with all above elements
const rExps = []
replace.forEach((element) => {
  rExps.push([new RegExp(element["name"], "g"), element["replaceWith"]])
})

var textNode;
let walk = document.createTreeWalker(document.body, NodeFilter.SHOW_TEXT, null, false);

// Replace in title
rExps.forEach(function (rExp) {
  document.title = document.title.replace(rExp[0], rExp[1]);
});

// Replace in body
while (textNode = walk.nextNode()) {
  rExps.forEach(function (rExp) {
    textNode.nodeValue = textNode.nodeValue.replace(rExp[0], rExp[1]);
  });
}
